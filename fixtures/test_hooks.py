import allure
import pytest
import yaml


def pytest_addoption(parser):
    parser.addoption("--conf",
                     action='store',
                     help="Path to the environment configuration")


def pytest_generate_tests(metafunc):
    config = None
    with open(metafunc.config.getoption("--conf")) as yaml_file:
        config = yaml.load(yaml_file)
    params = tuple(map(lambda browser: (config["login"],
                                        config["password"],
                                        config["github_login"],
                                        config["github_password"]),
                       config["browsers"]
                       )
                   )

    metafunc.parametrize("username, password, github_username, github_password", params)

@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"

    setattr(item, "rep_" + rep.when, rep)