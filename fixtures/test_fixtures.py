import allure
import pytest
import yaml

from config.test_conf import Config
from helpers.getters import get_browser, get_main_page

from browsers.browsers import Browsers
from page_objects.main_page import MainPage


@pytest.fixture(scope='session', autouse=True)
def init_drivers(request):
    config = None
    with open(request.config.getoption("--conf")) as yaml_file:
        config = yaml.load(yaml_file)

    Config.browsers = config["browsers"]
    Config.grid_address = config["grid_url"]

    def _get_browser(browser_name):
        Config.webdrivers[browser_name] = Browsers[browser_name]

    for br in Config.browsers:
        _get_browser(br)


@pytest.fixture
def main_page(request):
    next_brw = None
    if not Config.brw:
        Config.brw = get_browser()

    try:
        next_brw = Config.brw.__next__()
    except StopIteration:
        Config.brw = get_browser()
        next_brw = Config.brw.__next__()

    res = get_main_page(next_brw)

    return res

@pytest.fixture(autouse=True)
def test_result_check(request):
    yield
    main_page: MainPage = request.node.funcargs['main_page']
    if request.node.rep_setup.passed:
        if request.node.rep_call.failed:
            allure.attach(main_page.web_driver.get_screenshot_as_png(), "Web Page Screenshot", allure.attachment_type.PNG)

    main_page.web_driver.quit()